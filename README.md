Intro
-----

This is a small application to attempt the PHP Junior Challenge.


Youtube Presentation
-----

Please click the link below to check out the presentation on YouTube.

https://youtu.be/fN6R9CCRh3I

The Project
-----

### Installing Dependencies 
Run:

`$ composer install`

This will install all the dependencies associated with the project

### Running The Application
Run:

`$ php main.php`

To run the application.


### Running All Unit Tests

Make sure you have PHPUnit installed. 

When PHPUnit is installed, run:

`$ alias pu="php vendor/bin/phpunit"`

To create an alias to ease execution of phpunit then run:

`$ pu`

This will run all the test suites.

### Running Single Unit Tests

To run individual test suites:

`$ pu <TestPath>`

For example:

`$ pu tests/CamelCaseTest.php`

To run the CamelCaseTest suite.

### Running Specific Test Cases

To run specific a test run:

`$ pu <TestPath> --filter <TestCase>`

For example:

`$ pu tests/MinMaxSumTest.php --filter testSortArray`
