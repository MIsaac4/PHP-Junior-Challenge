<?php

require __DIR__.'/vendor/autoload.php';


use App\MinMaxSum;
use App\CamelCase;

echo "************************  MinMaxSum Test ***********************************\n";

echo "\n";

// MinMaxSum Test 
echo "Minimum and Maximum Sums for [9, 7, 5, 3, 1]\n";

echo "\n";

// MinMaxSum object
$minMaxSum = new MinMaxSum();

[$minSum, $maxSum] = $minMaxSum->minMax([9, 7, 5, 3, 1]);

// print minimum and maximum sums
echo "Minimum Sum: " . $minSum . "\nMaximum Sum: " . $maxSum . "\n\n";

echo "\n";

echo "************************  CamelCase Test ***********************************\n";

echo "\n";

echo "Number of Words in a CamelCase String ('saveChangesInTheEditor')\n";

echo "\n";

$camel = new CamelCase();

echo "Number of words: " . $camel->numberOfWords('saveChangesInTheEditor') . "\n";

echo "\n";

echo "*****************************************************************************\n";

echo "\n";


