<?php

namespace App;

class MinMaxSum 
{
    // minimum and maximum sums
    public int $minSum = 0;
    public int $maxSum = 0;

    public function minMax(array $array) {
        // sort array in ascending order
        $sortedArray = $this->sortArray($array);

        // the minimum sum
        $minSum = $this->minSum($sortedArray);
        // the maximum sum
        $maxSum = $this->maxSum($sortedArray);

        return [$minSum, $maxSum];
    }

    // Bubble sort helper
    public function sortArray(array $array): array {
        // for efficiency
        $length = count($array);

        for ($outer = 0; $outer < $length; $outer++) {
            for ($inner = 0; $inner < $length; $inner++) {
                if ($array[$outer] < $array[$inner]) {
                    $temp = $array[$outer];
                    $array[$outer] = $array[$inner];
                    $array[$inner] = $temp;
                }
            }
        }

        return $array;
    }

    // MinSum function
    public function minSum(array $array): int {
        foreach($array as $key => $value) {
            // skip the last (maximum) item in the array
            if ($key == (count($array) - 1)) {
                return $this->minSum;
            }
            $this->minSum += $value;
        }
    }

    // MaxSum function
    public function maxSum(array $array): int {
        foreach($array as $key => $value) {
            // skip the first (minimum) item in the array
            if ($key == 0) {
                continue;
            }
            $this->maxSum += $value;
        }

        return $this->maxSum;
    }
}


