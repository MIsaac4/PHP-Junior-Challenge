<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\CamelCase;


class CamelCaseTest extends TestCase
{
    // CamelCase object and camelCase test string
    protected $camelCase;
    protected $testCamelStr;

    protected function setUp(): void
    {
        // initialize object and test string before each test runs
        $this->camelCase = new CamelCase();
        $this->testCamelStr = 'saveChangesInTheEditor';
    }
    
    protected function tearDown(): void
    {
        // reset test string count  
        $this->camelCase->count = 1;
    }
    
    public function testNumberOfWordsInCamelCaseString()
    {
        // test whether the function returns the expected number of words in the test string
        $expected = 5;
        $actual = $this->camelCase->numberOfWords($this->testCamelStr);

        $this->assertEquals($expected, $actual);
    }

}
