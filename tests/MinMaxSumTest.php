<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\MinMaxSum;


class MinMaxSumTest extends TestCase
{
    // MinMaxSum object and test array
    protected $minMaxSum;
    protected $testArray;

    protected function setUp(): void
    {
        // initialize MinMaxSum object and test array
        $this->minMaxSum = new MinMaxSum();
        $this->testArray = [1, 3, 5, 7, 9];
    }

    protected function tearDown(): void
    {
        // reset the minimum and maximum sums
        $this->minMaxSum->minSum = 0;
        $this->minMaxSum->maxSum = 0;
    }
    
    public function testMinMaxSumReturnsExpectedValues()
    {
        // test whether the MinMax function returns the respective sums
        $expected = [16, 24];
        $actual = $this->minMaxSum->minMax($this->testArray);
        $this->assertEquals($expected, $actual);
    }

    public function testSortArray()
    {
        // test whether the sort helper function sorts the array in ascending order
        $expected = [1, 3, 5, 7, 9];
        $unsortedArray = [9, 5, 3, 1, 7];
        $actual = $this->minMaxSum->sortArray($unsortedArray);

        $this->assertEquals($expected, $actual);
    }
}
